import org.junit.jupiter.api.Test
import kotlin.test.expect

class H2Test {
    @Test fun h2() {
        h2 {
            Person(name = "Jon Lord", age = 42).save()
            expect(listOf("Jon Lord")) { Person.findAll().map { it.name } }
        }
    }
}
