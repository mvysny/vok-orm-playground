import com.github.vokorm.KEntity
import com.gitlab.mvysny.jdbiorm.Dao
import org.hibernate.validator.constraints.Length
import org.jdbi.v3.core.mapper.reflect.ColumnName
import java.time.Instant
import java.time.LocalDate

data class Person(
        override var id: Long? = null,
        @field:Length(min = 1)
        var name: String = "",
        var age: Int = 0,
        var dateOfBirth: LocalDate? = null,
        var created: Instant? = null,
        var modified: Instant? = null,
        // test of aliased field
        @field:ColumnName("alive")
        var isAlive: Boolean? = null,
        var maritalStatus: MaritalStatus? = null

) : KEntity<Long> {
    override fun save(validate: Boolean) {
        if (id == null) {
            created = Instant.now()
        }
        modified = Instant.now()
        super.save(validate)
    }

    companion object : Dao<Person, Long>(Person::class.java)
}

enum class MaritalStatus {
    Single,
    Married,
    Divorced,
    Widowed
}
