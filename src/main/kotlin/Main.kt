import java.time.LocalDate

/**
 * Edit this method freely. See [vok-orm](https://github.com/mvysny/vok-orm) for documentation on how to query database and
 * insert stuff.
 */
fun main() {

    // PostgreSQL 10.3:
    // 1. Run in docker: docker run --rm -ti -e POSTGRES_PASSWORD=mysecretpassword -p 127.0.0.1:5432:5432 postgres:10.3
    // 2. do stuff in  `postgreSQL {}`


    // MariaDB 10.1.31:
    // 1. Run in docker: docker run --rm -ti -e MYSQL_ROOT_PASSWORD=mysqlpassword -e MYSQL_DATABASE=db -e MYSQL_USER=testuser -e MYSQL_PASSWORD=mysqlpassword -p 127.0.0.1:3306:3306 mariadb:10.1.31
    // 2. do stuff in  `mariadb {}`


    // MySQL:
    // 1. Run in docker: docker run --rm -ti -e MYSQL_ROOT_PASSWORD=mysqlpassword -e MYSQL_DATABASE=db -e MYSQL_USER=testuser -e MYSQL_PASSWORD=mysqlpassword -p 127.0.0.1:3306:3306 mysql:5.7.21
    // 2. do stuff in  `mysql {}`


    // H2:
    // 1. Embedded H2 is started automatically in in-memory mode; changes are not persisted and are lost.
    // 2. do stuff in  `h2 {}`


    // MSSQL 2017 Express:
    // 1. Run in docker: docker run --rm -ti -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=myPASSWD123" -p 1433:1433 --name sqlserver mcr.microsoft.com/mssql/server:2017-latest-ubuntu
    // 2. do stuff in  `mssql {}`


    // CockroachDB:
    // 1. Run in docker: docker run --rm -ti -p26257:26257 cockroachdb/cockroach start-single-node --insecure
    // 2. do stuff in  `cockroachDB(() -> {});`

    h2 {
        Person(name = "Jon Lord", age = 42, dateOfBirth = LocalDate.of(1941, 6, 9), isAlive = false, maritalStatus = MaritalStatus.Married).save()
        println(Person.findAll())
    }
}
