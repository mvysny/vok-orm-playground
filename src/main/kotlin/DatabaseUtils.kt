import com.github.vokorm.*
import com.gitlab.mvysny.jdbiorm.JdbiOrm
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.intellij.lang.annotations.Language

fun ddl(@Language("sql") sql: String) {
    db { handle.createUpdate(sql).execute() }
}

/**
 * Experiment on PostgreSQL. Run PostgreSQL in Docker simply:
 * ```
 * docker run --rm -ti -e POSTGRES_PASSWORD=mysecretpassword -p 127.0.0.1:5432:5432 postgres:10.3
 * ```
 */
fun postgreSQL(block: () -> Unit) {
    val cfg = HikariConfig().apply {
        jdbcUrl = "jdbc:postgresql://localhost:5432/postgres"
        username = "postgres"
        password = "mysecretpassword"
    }
    JdbiOrm.setDataSource(HikariDataSource(cfg))
    try {
        ddl("""create table if not exists Person (
                id bigserial primary key,
                name varchar(400) not null,
                age integer not null,
                dateOfBirth date,
                created timestamp,
                modified timestamp,
                alive boolean,
                maritalStatus varchar(200)
                 )""")
        block()
    } finally {
        JdbiOrm.destroy()
    }
}

/**
 * Starts an in-memory H2 database. After the main() method finishes, the database is gone, along with its contents.
 */
fun h2(block: () -> Unit) {
    val cfg = HikariConfig().apply {
        jdbcUrl = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1"
        username = "sa"
        password = ""
    }
    JdbiOrm.setDataSource(HikariDataSource(cfg))
    try {
        ddl("""create table if not exists Person (
                id bigint primary key auto_increment,
                name varchar not null,
                age integer not null,
                dateOfBirth date,
                created timestamp,
                modified timestamp,
                alive boolean,
                maritalStatus varchar
                 )""")
        block()
    } finally {
        JdbiOrm.destroy()
    }
}

/**
 * Experiment on MariaDB. Run MariaDB 10.1.31 in Docker simply:
 * ```
 * docker run --rm -ti -e MYSQL_ROOT_PASSWORD=mysqlpassword -e MYSQL_DATABASE=db -e MYSQL_USER=testuser -e MYSQL_PASSWORD=mysqlpassword -p 127.0.0.1:3306:3306 mariadb:10.1.31
 * ```
 */
fun mariadb(block: () -> Unit) {
    val cfg = HikariConfig().apply {
        jdbcUrl = "jdbc:mariadb://localhost:3306/db"
        username = "testuser"
        password = "mysqlpassword"
    }
    JdbiOrm.setDataSource(HikariDataSource(cfg))
    try {
        ddl(
                """create table if not exists Person (
                id bigint primary key auto_increment,
                name varchar(400) not null,
                age integer not null,
                dateOfBirth date,
                created timestamp(3) NULL,
                modified timestamp(3) NULL,
                alive boolean,
                maritalStatus varchar(200)
                 )"""
        )
        block()
    } finally {
        JdbiOrm.destroy()
    }
}

/**
 * Experiment on MySQL. Run MySQL 5.7.21 in Docker simply:
 * ```
 * docker run --rm -ti -e MYSQL_ROOT_PASSWORD=mysqlpassword -e MYSQL_DATABASE=db -e MYSQL_USER=testuser -e MYSQL_PASSWORD=mysqlpassword -p 127.0.0.1:3306:3306 mysql:5.7.21
 * ```
 */
fun mysql(block: () -> Unit) {
    val cfg = HikariConfig().apply {
        jdbcUrl = "jdbc:mysql://localhost:3306/db"
        username = "testuser"
        password = "mysqlpassword"
    }
    JdbiOrm.setDataSource(HikariDataSource(cfg))
    try {
        ddl("""create table if not exists Person (
                id bigint primary key auto_increment,
                name varchar(400) not null,
                age integer not null,
                dateOfBirth date,
                created timestamp(3) NULL,
                modified timestamp(3) NULL,
                alive boolean,
                maritalStatus varchar(200)
                 )""")
        block()
    } finally {
        JdbiOrm.destroy()
    }
}

/**
 * Experiment on Microsoft SQL. Run MSSQL 2017 Express in Docker simply:
 * ```
 * docker run --rm -ti -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=myPASSWD123" -p 1433:1433 --name sqlserver mcr.microsoft.com/mssql/server:2017-latest-ubuntu
 * ```
 */
fun mssql(block: () -> Unit) {
    val cfg = HikariConfig().apply {
        jdbcUrl = "jdbc:sqlserver://localhost:1433;database=tempdb"
        username = "sa"
        password = "myPASSWD123"
    }
    JdbiOrm.setDataSource(HikariDataSource(cfg))
    try {
        ddl("""IF OBJECT_ID('Person', 'U') IS NOT NULL DROP TABLE Person;""")
        ddl("""create table Person (
                id bigint primary key IDENTITY(1,1) not null,
                name varchar(400) not null,
                age integer not null,
                dateOfBirth datetime NULL,
                created datetime NULL,
                modified datetime NULL,
                alive bit,
                maritalStatus varchar(200)
                 )""")
        block()
    } finally {
        JdbiOrm.destroy()
    }
}

/**
 * Experiment on [CockroachDB](https://www.cockroachlabs.com/). Run CockroachDB in Docker simply:
 * ```
 * docker run --rm -ti -p26257:26257 cockroachdb/cockroach start-single-node --insecure
 * ```
 */
fun cockroachDB(block: () -> Unit) {
    val cfg = HikariConfig().apply {
        jdbcUrl = "jdbc:postgresql://127.0.0.1:26257/defaultdb?sslmode=disable"
        username = "root"
        password = ""
    }
    JdbiOrm.setDataSource(HikariDataSource(cfg))
    try {
        ddl("""create table if not exists Person (
                id bigserial primary key,
                name varchar(400) not null,
                age integer not null,
                dateOfBirth date,
                created timestamp,
                modified timestamp,
                alive boolean,
                maritalStatus varchar(200)
                 )""")
        block()
    } finally {
        JdbiOrm.destroy()
    }
}

