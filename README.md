[![pipeline status](https://gitlab.com/mvysny/vok-orm-playground/badges/master/pipeline.svg)](https://gitlab.com/mvysny/vok-orm-playground/commits/master)

# `vok-orm` Playground

Allows for easy experimenting with the [vok-orm](https://github.com/mvysny/vok-orm)
library.
Simply clone this project, open it in your IDE and start experimenting. Requires Java 11+.

```bash
git clone https://gitlab.com/mvysny/vok-orm-playground
```

Simply run the [Main.kt](src/main/kotlin/Main.kt) main class, to access
the in-memory embedded H2. The `Main.kt` file contains instructions on how
to connect to MySQL, MariaDB and PostgreSQL, and even how to run them
in Docker, to ease the setup ramp-up.

The default `main` simply launches embedded H2.

To experiment without an IDE, simply run `main` straight from your command-line:

```bash
./gradlew run
```

Alternatively, build a runnable project and run it via a script:

```bash
./gradlew
cd build/distributions
unzip *.zip
cd vok-orm-playground/bin
./vok-orm-playground
```

> For a Java-based project please head out to [JDBI-ORM Playground](https://gitlab.com/mvysny/jdbi-orm-playground/).

## Experimenting

A `Person` entity is pre-provided for you. It consists of the [Person](src/main/kotlin/Person.kt) Kotlin class,
and the database table set up by the DDL script present in the [DatabaseUtils.kt](src/main/kotlin/DatabaseUtils.kt)
file.

In order to add another entity, simply do these two things:
* Create a DDL script for the entity and add it to appropriate function in the `DatabaseUtils.kt` file.
  For example, for the MySQL database just add the DDL into the `mysql` function.
* Then, create the entity as a Kotlin class, according to the [vok-orm documentation](https://github.com/mvysny/vok-orm).
  In short you need to create a field for every column, then make your entity class
  implement the `Entity` interface, and create a `companion object` which implements the `Dao` interface.
  When in doubt, see the [Person](src/main/kotlin/Person.kt) class for examples.

## Running With Other Databases

By default the Playground runs with embedded H2. The JDBC connection parameters to this database are as follows:

```
jdbcUrl = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1"
username = "sa"
password = ""
```

To run with other kinds of databases
please read below.

### PostgreSQL 10.3

Start PostgreSQL in Docker:

```bash
docker run --rm -ti -e POSTGRES_PASSWORD=mysecretpassword -p 127.0.0.1:5432:5432 postgres:10.3
```

Then, run the following `main` method:

```kotlin
fun main() {
    postgreSQL {
        Person(name = "Jon Lord", age = 42).save()
        println(Person.findAll())
    }
}
```

It should print one person:

```bash
[Person(id=1, name=Jon Lord, age=42, dateOfBirth=null, created=2018-12-07T08:57:17.623Z, modified=2018-12-07T08:57:17.623Z, isAlive=null, maritalStatus=null)]
```

Press `Ctrl+C` in the docker console to kill PostgreSQL and remove it completely.

The JDBC connection parameters to this database are as follows:

```
jdbcUrl = "jdbc:postgresql://localhost:5432/postgres"
username = "postgres"
password = "mysecretpassword"
```

### MariaDB 10.1.31

Start MariaDB in Docker:

```bash
docker run --rm -ti -e MYSQL_ROOT_PASSWORD=mysqlpassword -e MYSQL_DATABASE=db -e MYSQL_USER=testuser -e MYSQL_PASSWORD=mysqlpassword -p 127.0.0.1:3306:3306 mariadb:10.1.31
```

Then, run the following `main` method:

```kotlin
fun main() {
    mariadb {
        Person(name = "Jon Lord", age = 42).save()
        println(Person.findAll())
    }
}
```

It should print one person:

```bash
[Person(id=1, name=Jon Lord, age=42, dateOfBirth=null, created=2018-12-07T08:57:17.623Z, modified=2018-12-07T08:57:17.623Z, isAlive=null, maritalStatus=null)]
```

MariaDB ignores `Ctrl+C` in the docker console. To kill it, run `docker ps` to
list all docker containers currently running, then use `docker kill` to kill it:

```bash
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                      NAMES
08df322270ff        mariadb:10.1.31     "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        127.0.0.1:3306->3306/tcp   xenodochial_joliot
$ docker kill xenodochial_joliot
```

The JDBC connection parameters to this database are as follows:

```
jdbcUrl = "jdbc:mariadb://localhost:3306/db"
username = "testuser"
password = "mysqlpassword"
```

### MySQL

Start MySQL in Docker:

```bash
docker run --rm -ti -e MYSQL_ROOT_PASSWORD=mysqlpassword -e MYSQL_DATABASE=db -e MYSQL_USER=testuser -e MYSQL_PASSWORD=mysqlpassword -p 127.0.0.1:3306:3306 mysql:8.0.25
```

Then, run the following `main` method:

```kotlin
fun main() {
    mysql {
        Person(name = "Jon Lord", age = 42).save()
        println(Person.findAll())
    }
}
```

It should print one person:

```bash
[Person(id=1, name=Jon Lord, age=42, dateOfBirth=null, created=2018-12-07T08:57:17.623Z, modified=2018-12-07T08:57:17.623Z, isAlive=null, maritalStatus=null)]
```

MySQL ignores `Ctrl+C` in the docker console. To kill it, run `docker ps` to
list all docker containers currently running, then use `docker kill` to kill it:

```bash
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                      NAMES
62859c682e9d        mysql:5.7.21        "docker-entrypoint.s…"   4 minutes ago       Up 4 minutes        127.0.0.1:3306->3306/tcp   determined_carson
$ docker kill determined_carson
```

The JDBC connection parameters to this database are as follows:

```
jdbcUrl = "jdbc:mysql://localhost:3306/db"
username = "testuser"
password = "mysqlpassword"
```

### Microsoft SQL 2017 Express

Start MSSQL in Docker:

```bash
docker run --rm -ti -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=myPASSWD123" -p 1433:1433 --name sqlserver mcr.microsoft.com/mssql/server:2017-latest-ubuntu
```

Then, run the following `main` method:

```kotlin
fun main() {
    mssql {
        Person(name = "Jon Lord", age = 42).save()
        println(Person.findAll())
    }
}
```

It should print one person:

```bash
[Person(id=1, name=Jon Lord, age=42, dateOfBirth=null, created=2018-12-07T08:57:17.623Z, modified=2018-12-07T08:57:17.623Z, isAlive=null, maritalStatus=null)]
```

Press `Ctrl+C` in the docker console to kill MSSQL and remove it completely.

The JDBC connection parameters to this database are as follows:

```
jdbcUrl = "jdbc:sqlserver://localhost:1433;database=tempdb"
username = "sa"
password = "myPASSWD123"
```

### CockroachDB

Start CockroachDB in Docker:

```bash
docker run --rm -ti -p26257:26257 cockroachdb/cockroach start-single-node --insecure
```

Then, run the following `main` method:

```kotlin
fun main() {
  cockroachDB {
    Person(name = "Jon Lord", age = 42).save()
    println(Person.findAll())
  }
}
```

It should print one person:

```bash
[Person(id=1, name=Jon Lord, age=42, dateOfBirth=null, created=2018-12-07T08:57:17.623Z, modified=2018-12-07T08:57:17.623Z, isAlive=null, maritalStatus=null)]
```

To kill CockroachDB, type `\q` into the console then press `CTRL+C`.

The JDBC connection parameters to this database are as follows:

```
jdbcUrl = "jdbc:postgresql://127.0.0.1:26257/defaultdb?sslmode=disable"
username = "root"
password = ""
```

CockroachDB uses the same communication protocol as PostgreSQL does, therefore
you use PostgreSQL JDBC driver to communicate with CockroachDB.
